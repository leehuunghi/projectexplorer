﻿// 1512349-Explorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512349-Explorer.h"

#define MAX_PATH_LENGTH 10000
#define MAX_LOADSTRING 100
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

//Hàm con
void LoadDriver(HTREEITEM& root);
void LoadChildTreeView(HTREEITEM &hParent);
void LoadChildListView(LPCWSTR item);
LPCWSTR GetPathListView(LPCWSTR path);

#define BUFFER_LEN 255

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1512349EXPLORER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512349EXPLORER));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1512349EXPLORER));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1512349EXPLORER);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
HWND gTreeView, gListView;
HTREEITEM hRoot;
WCHAR buffer[BUFFER_LEN];

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
					  //get size window
					  RECT rWindow;
					  GetWindowRect(hWnd, &rWindow);

					  INITCOMMONCONTROLSEX icex;

					  // Ensure that the common control DLL is loaded. 
					  icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
					  icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
					  InitCommonControlsEx(&icex);

					  gListView = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
						  WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER,
						  rWindow.right / 3, 0, rWindow.right, rWindow.bottom, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);

					  gTreeView = CreateWindowEx(NULL, WC_TREEVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_BORDER | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS, 0, 0, rWindow.right / 3, rWindow.bottom, hWnd, (HMENU)IDC_TREEVIEW, hInst, NULL);


					  // tạo cột list view
					  LVCOLUMN lvCol;

					  lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
					  lvCol.fmt = LVCFMT_LEFT;
					  lvCol.pszText = L"Tên";
					  lvCol.cx = 100;
					  ListView_InsertColumn(gListView, 0, &lvCol);

					  lvCol.fmt = LVCFMT_RIGHT;
					  lvCol.cx = 80;
					  lvCol.pszText = L"Loại";
					  ListView_InsertColumn(gListView, 1, &lvCol);

					  lvCol.cx = 200;
					  lvCol.pszText = L"Thời gian chỉnh sửa";
					  ListView_InsertColumn(gListView, 2, &lvCol);

					  lvCol.cx = 200;
					  lvCol.pszText = L"Dung lượng";
					  ListView_InsertColumn(gListView, 3, &lvCol);

					  // Chèn node gốc tree view
					  TV_INSERTSTRUCT tvInsert;
					  tvInsert.hParent = NULL;
					  tvInsert.hInsertAfter = TVI_ROOT;
					  tvInsert.item.mask = TVIF_TEXT | TVIF_CHILDREN;
					  tvInsert.item.pszText = L"This PC";
					  tvInsert.item.lParam = (LPARAM)L"This PC";
					  tvInsert.item.cChildren = 1;
					  hRoot = TreeView_InsertItem(gTreeView, &tvInsert);

					  //chèn ổ đĩa vào list view và tree view
					  LoadDriver(hRoot);

					  break;
	}
	case WM_NOTIFY:
	{
					  int wmID = LOWORD(wParam);

					  switch (wmID)
					  {
					  case IDC_TREEVIEW:
					  {
										   LPNMHDR info = (LPNMHDR)lParam;
										   if(NM_DBLCLK == info->code) {

											   HTREEITEM item;
											   item = TreeView_GetNextItem(gTreeView,-1, TVGN_CARET);
											   if (item != hRoot &&item!=NULL)
											   {
												   LoadChildTreeView(item);
											   }
										   }
										   break;
					  }
					  case IDC_LISTVIEW:
					  {
										   LPNMHDR info = (LPNMHDR)lParam;
										   if (NM_DBLCLK == info->code) {
											   int index = ListView_GetNextItem(gListView, -1, LVNI_FOCUSED);
											   
											   LVITEM item;
											   item.mask = LVIF_PARAM;
											   item.iSubItem = 0;
											   item.lParam = (LPARAM)buffer;
											   item.cchTextMax = 255;
											   item.iItem = index;

											   ListView_GetItem(gListView, &item);

											   LoadChildListView((LPCWSTR)item.lParam);
										   }

					  }
						  break;
					  }
					  break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			MessageBox(0, L"write by Le HuuNghi\nMSSV:1512349",0,1);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void LoadDriver(HTREEITEM& root)
{
	LV_ITEM lv;
	int index;
	TV_INSERTSTRUCT tvInsert;
	WCHAR *filePath;

	int countDriver = 0, i = 0 , j, k;
	GetLogicalDriveStrings(BUFFER_LEN, buffer);

	//Đếm số lượng ổ đĩa 
	for (i = 0; !((buffer[i] == 0) && (buffer[i + 1] == 0)); ++i)
	if (buffer[i] == 0)
		++countDriver;
	++countDriver;

	WCHAR* bufferContaintNameDriver;
	//Lấy từng kí tự ổ đĩa

	i = 0;

	for (j = 0; j< countDriver; ++j)
	{
		bufferContaintNameDriver = new WCHAR[10];
		index = k = 0;
		while (buffer[i] != 0)
			bufferContaintNameDriver[k++] = buffer[i++];
		bufferContaintNameDriver[k-1] = 0; //Kết thúc chuỗi
		filePath = new WCHAR[255];
		StrCpy(filePath, bufferContaintNameDriver);
		//push vào list view
		lv.mask = LVIF_TEXT | TVIF_PARAM;
		lv.iItem = j;
		lv.iSubItem = 0;
		lv.lParam = (LPARAM)filePath;
		lv.pszText = bufferContaintNameDriver;
		ListView_InsertItem(gListView, &lv);

		//push vào tree view
		tvInsert.hParent = root;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.mask = TVIF_TEXT | TVIF_CHILDREN|TVIF_PARAM;
		tvInsert.item.cChildren = 1;
		tvInsert.item.lParam = (LPARAM)filePath;
		tvInsert.item.pszText = bufferContaintNameDriver;
		HTREEITEM hDrive = TreeView_InsertItem(gTreeView, &tvInsert);
		++i;
	}
}

LPCWSTR GetPath(HTREEITEM hItem)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(gTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

void LoadChildTreeView(HTREEITEM &hParent)
{
	WCHAR* buffer, *buffer1;
	WCHAR *path;

	buffer = new WCHAR[MAX_PATH_LENGTH];
	buffer1 = new WCHAR[MAX_PATH_LENGTH];
	StrCpy(buffer, GetPath(hParent));

	if (wcslen(buffer) == 3) //Nếu quét các ổ đĩa
	{
		if (StrStr(buffer, _T("A:")) || StrStr(buffer, _T("B:"))) //Đĩa mềm hổng làm 
			return;
	}
	else
		StrCat(buffer, _T("\\"));
	StrCpy(buffer1, buffer);
	StrCat(buffer, _T("*"));

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);

	if (hFile == INVALID_HANDLE_VALUE)
		return;

	BOOL bFound = true;
	bool flag = false;
	//Trong khi còn tìm thấy file hay thư mục
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			path = new WCHAR[MAX_PATH];
			StrCpy(path, buffer1);
			TV_INSERTSTRUCT tvInsert;
			tvInsert.hParent = hParent;
			tvInsert.hInsertAfter = TVI_LAST;
			tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM|TVIF_CHILDREN;
			tvInsert.item.pszText = fd.cFileName;
			tvInsert.item.cChildren = true;
			StrCat(path, fd.cFileName);
			tvInsert.item.lParam = (LPARAM)path;
			TreeView_InsertItem(gTreeView, &tvInsert);
			flag = true;
		}
		bFound = FindNextFileW(hFile, &fd);
		
	}//while
	if (!flag)
	{
		TVITEMEX tv;
		tv.mask = TVIF_CHILDREN;
		tv.hItem = hParent;
		tv.cChildren = false;
		TreeView_SetItem(gTreeView,&tv);
	}
	delete[]buffer;
	delete[]buffer1;
}

LPCWSTR GetPathListView(int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(gListView, &lv);
	return (LPCWSTR)lv.lParam;
}

LPWSTR _GetDateModified(const FILETIME &ftLastWrite)
{

	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d %s"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour>12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour>12) ? (_T("Chiều")) : (_T("Sáng")));

	return buffer;
}

#define KB 1
#define MB 2
#define GB 3
#define TB 4
#define RADIX 10

LPWSTR Convert(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, RADIX);

	if (nRight != 0 && nType > KB)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, RADIX);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" bytes"));
		break;
	case KB:
		StrCat(buffer, _T(" KB"));
		break;
	case MB:
		StrCat(buffer, _T(" MB"));
		break;
	case GB:
		StrCat(buffer, _T(" GB"));
		break;
	case TB:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}


LPWSTR _GetSize(const WIN32_FIND_DATA &fd)
{
	DWORD dwSize = fd.nFileSizeLow;

	return Convert(dwSize);
}

#define ENTIRE_STRING NULL

LPWSTR _GetType(const WIN32_FIND_DATA &fd)
{
	int nDotPos = StrRStrI(fd.cFileName, ENTIRE_STRING, _T(".")) - fd.cFileName;
	int len = wcslen(fd.cFileName);

	if (nDotPos < 0 || nDotPos >= len) //Nếu không tìm thấy
		return _T("Không biết");

	TCHAR *szExtension = new TCHAR[len - nDotPos + 1];
	int i;

	for (i = nDotPos; i < len; ++i)
		szExtension[i - nDotPos] = fd.cFileName[i];
	szExtension[i - nDotPos] = NULL; //Kí tự kết thúc chuỗi

	if (!StrCmpI(szExtension, _T(".htm")) || !StrCmpI(szExtension, _T(".html")))
	{
		return _T("Web page");
	}
	TCHAR pszOut[256];
	HKEY hKey;
	DWORD dwType = REG_SZ;
	DWORD dwSize = 256;

	//Kiếm handle của extension tương ứng trong registry
	if (RegOpenKeyEx(HKEY_CLASSES_ROOT, szExtension, 0, KEY_READ, &hKey) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Không biết");
	}

	if (RegQueryValueEx(hKey, NULL, NULL, &dwType, (PBYTE)pszOut, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Không biết");
	}
	RegCloseKey(hKey);

	//Kiếm mô tả về thông tin của extension thông qua handle của key tương ứng trong registry
	TCHAR *pszPath = new TCHAR[1000];
	dwSize = 1000;
	if (RegOpenKeyEx(HKEY_CLASSES_ROOT, pszOut, 0, KEY_READ, &hKey) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Không biết");
	}

	if (RegQueryValueEx(hKey, NULL, NULL, &dwType, (PBYTE)pszPath, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Không biết");
	}
	RegCloseKey(hKey);

	return pszPath;
}


void LoadChildListView(LPCWSTR path)
{
	ListView_DeleteAllItems(gListView);
	WCHAR *buffer = new WCHAR[10240];
	StrCpy(buffer, path);

	if (wcslen(buffer) == 3) //Nếu quét các ổ đĩa
		StrCat(buffer, _T("*"));
	else
		StrCat(buffer, _T("\\*"));

	//Bắt đầu tìm các file và folder trong thư mục
	WIN32_FIND_DATA fd;
	HANDLE hFile;
	BOOL bFound = true;
	LV_ITEM lv;

	TCHAR * folderPath;
	int nItemCount = 0;

	//Chạy lần thứ nhất lấy các thư mục
	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(folderPath, path);

			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));

			StrCat(folderPath, fd.cFileName);

			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)folderPath;
			ListView_InsertItem(gListView, &lv);

			//Bỏ qua cột thứ hai là Size (cho giống Explorer, chỉ hiển thị dung lượng
			//của thư mục ở thanh status bar thôi

			//Cột thứ hai là cột Type
			ListView_SetItemText(gListView, nItemCount, 1, _T("Thư mục"));

			//Cột thứ ba là cột Date modified
			ListView_SetItemText(gListView, nItemCount, 2, _GetDateModified(fd.ftLastWriteTime));
			++nItemCount;
		}//if

		bFound = FindNextFileW(hFile, &fd);
	}//while

	DWORD folderCount = nItemCount;
	/*************************************************************************************/

	TCHAR *filePath;
	DWORD fileSizeCount = 0;
	DWORD fileCount = 0;

	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			filePath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(filePath, path);

			if (wcslen(path) != 3)
				StrCat(filePath, _T("\\"));

			StrCat(filePath, fd.cFileName);

			//Cột thứ nhất là tên hiển thị của tập tin
			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)filePath;
			ListView_InsertItem(gListView, &lv);

			//Cột thứ tư là Size
			ListView_SetItemText(gListView, nItemCount, 3, _GetSize(fd));
			fileSizeCount += fd.nFileSizeLow;

			//Cột thứ hai là Type
			ListView_SetItemText(gListView, nItemCount, 1, _GetType(fd));

			//Cột thứ ba là Date Modified	
			ListView_SetItemText(gListView, nItemCount, 2, _GetDateModified(fd.ftLastWriteTime));

			++nItemCount;
			++fileCount;
		}//if

		bFound = FindNextFileW(hFile, &fd);
	}//while
	delete[]buffer;
}