1. Lê Hữu Nghị - 1512349 - leehuunghi@gmail.com
2. Các chức năng làm được:
	* Tạo ra TreeView bên trái, ListView bên phải. 
	* Xét TreeView:
		+ Tạo node root là This PC
		+ Lấy danh sách các ổ đĩa trong máy bằng hàm GetLogicalDrives hoặc GetLogicalDriveStrings, thêm các ổ đĩa vào node root,
		   tạo sẵn thuộc tính cChildren = true để báo hiệu có các node con. Gán giá trị của ổ đĩa ví dụ C:\ vào PARAM (tag) để lấy lên xài lại.
		+ Bắt sự kiện Expanding, lấy ra đường dẫn dấu ở PARAM để biết mình phải xư lí thư mục nào,
		    duyệt nội dung thư mục bằng FindFirstFile & FindNextFile, chỉ lấy các thư mục để thêm vào làm node con.
	* Xét ListView:
		+ Hiển thị toàn bộ thư mục và tập tin tương ứng với một đường dẫn
		+ Bấm đôi vào một thư mục sẽ thấy toàn bộ thư mục con và tập tin.
		+ Tạo ra ListView có 4 cột: Tên, Loại, Thời gian chỉnh sửa, Dung lượng.
3. Các Luồng sự kiện chính (Main success scenario):
	* Chạy chương trình lên, hiển thị node This PC trên TreeView bên trái ở trạng thái collapse (thu gọn).
	    Bấm vào sẽ xổ xuống các node con là danh sách ổ đĩa.
	* Bấm vào ổ đĩa C đang ở trạng thái collapse(thu gọn) trong TreeView bên trái sẽ xổ xuống danh sách các thư mục con.
	* Bấm vào ổ đĩa hoặc thư mục của ListView bên phải thì sẽ hiện ra các tập tin và thư mục con.
4. Các luồng sự kiện phụ:
	* Khi bấm vào treeview bên trái thì mở thư mục đến cấp 6 7 thì không mở được nữa.
	* Khi bấm vào tập tin bên listview bên phải thì sẽ ra trống ( thiếu shell excute)
	* Khi thư mục không có con vẫn hiện dấu '+' của treeview bên trái
5. link bitbucket: https://leehuunghi@bitbucket.org/leehuunghi/projectexplorer.git